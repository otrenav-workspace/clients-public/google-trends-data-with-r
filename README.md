
| [Website](http://links.otrenav.com/website) | [Twitter](http://links.otrenav.com/twitter) | [LinkedIn](http://links.otrenav.com/linkedin)  | [GitHub](http://links.otrenav.com/github) | [GitLab](http://links.otrenav.com/gitlab) | [CodeMentor](http://links.otrenav.com/codementor) |

---

# Google Trends Data with R

- Omar Trejo
- January, 2017

## Objective

Extract data from Google Trends using R.

## Considerations

There's an unpublished threshold limit for API calls for Google Trends data.
That's why a variety of functions are provided to try to deal with this. However
the limit seems to be sticky for quite a bit of time.

---

> "The best ideas are common property."
>
> —Seneca
